Rails.application.routes.draw do
  if Rails.env.development?
    mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "/graphql"
  end
  post "/graphql", to: "graphql#execute"
  root to: 'activities#index'

  resources :activities
  resources :categories
  resources :shelves

  resources :series, only: [:index, :show]
  resources :movies, only: [:index, :show]

  resources :search, only: [:index]
end
