module Mutations
  class CreateActivity < BaseMutation
    # Todo argumento que debe recibir el método resolve
    argument :name, String, required: true
    argument :description, String, required: true

    # El tipo a retornar
    type Types::ActivityType

    def resolve(name:, description:)
      Activity.create!(
        name: name,
        description: description
      )
    end
  end
end
