class SeriesController < ApplicationController
  def index
    @series = Category.find_by(name: 'Series')&.activities
  end

  def show
    @serie = Activity.find(params[:id])
  end
end
