class CategoriesController < ApplicationController
  def index
    @categories = Category.all
    @category = Category.new
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)

    respond_to do |format|
      if @category.save
        format.html { redirect_to categories_url, notice: 'Categoría creada' }
      else
        format.html { render :new }

        # Esta es la forma en que le decimos a ActionCable cómo transmitir
        # el formulario de nuevo pero con los errores incluídos al turbo_frame_tag
        # correcto.
        #
        # Para la acción exitosa no es necesario.
        format.turbo_stream do
          render(
            turbo_stream: turbo_stream.replace(
              @category,
              partial: 'categories/form',
              locals: { category: @category }
            )
          )
        end
      end
    end
  end

  def edit
    @category = Category.find(params[:id])
  end

  def update
    @category = Category.find(params[:id])

    if @category.update(category_params)
      redirect_to categories_url, notice: 'Categoría actualizada'
    else
      render :edit
    end
  end

  def show
    @category = Category.find(params[:id])
  end

  private

  def category_params
    params.require(:category).permit(:name, :description, :cover)
  end
end
