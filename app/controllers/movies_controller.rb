class MoviesController < ApplicationController
  # skip_before_action :prevent_htmx!

  def index
    @movies = Category.find_by(name: 'Películas')&.activities
  end

  def show
    # prevent_htmx!

    @movie = Activity.find(params[:id])

    respond_to do |format|
      format.html
    end
  end
end
