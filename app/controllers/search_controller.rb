class SearchController < ApplicationController
  def index
    @activities = if params[:query].present?
                    Activity.where("LOWER(name) LIKE '%#{params[:query]}%'")
                  else
                    Activity.order(created_at: :desc)
                  end

    respond_to do |format|
      format.html do
        render 'activities/index'
      end
    end
  end
end
