class ShelvesController < ApplicationController
  def index
    @shelves = Shelf.all
    @shelf = Shelf.new
  end

  def show
    @shelf = Shelf.find(params[:id])
  end

  def new
    @shelf = Shelf.new
  end

  def create
    @shelf = Shelf.new(shelf_params)

    respond_to do |format|
      if @shelf.save
        format.html { redirect_to shelves_url, notice: 'Estante creado' }
      else
        format.html { render :new }

        format.turbo_stream do
          render(
            turbo_stream: turbo_stream.replace(
              @shelf,
              partial: 'shelves/form',
              locals: { shelf: @shelf }
            )
          )
        end
      end
    end
  end

  def edit
    @shelf = Shelf.find(params[:id])
  end

  def update
    @shelf = Shelf.find(params[:id])

    if @shelf.update(shelf_params)
      redirect_to shelves_url, notice: 'Estante actualizado'
    else
      render :edit
    end
  end

  private

  def shelf_params
    params.require(:shelf).permit(:name, :description, :cover)
  end
end
