# frozen_string_literal: true

# Tiene que llamarse así porque sino choca con ApplicationComponent
# generado por la gema ViewComponent
class ApplicationPhlexComponent < Phlex::HTML
	include Phlex::Rails::Helpers::Routes
  include Phlex::Rails::Helpers::LinkTo
  include Phlex::Rails::Helpers::ImageTag

	if Rails.env.development?
		def before_template
			comment { "Before #{self.class.name}" }
			super
		end
	end
end
