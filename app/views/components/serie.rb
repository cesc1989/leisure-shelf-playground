class Serie < ApplicationPhlexComponent
  def initialize(serie)
    @serie = serie
  end

  def template
    link_to series_path(@serie.id) do
      div(class: "max-w-xs rounded overflow-hidden my-2 mr-2 bg-gray-200") do
        image_tag('books.jpg', class: 'w-full h-32')

        div(class: "px-6 py-4") do
          div(class: "name font-bold text-xl mb-2") do
            "#{@serie.name}"
          end

          p(class: "text-grey-darker text-base") do
            "#{@serie.description}"
          end
        end
      end
    end
  end
end
