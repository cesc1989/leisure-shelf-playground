class Series < ApplicationPhlexComponent
  def initialize(series:)
    @series = series
  end

  def template
    @series.each do |s|
      render Serie.new(s)
    end
  end
end
