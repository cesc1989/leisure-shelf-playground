# frozen_string_literal: true

class ApplicationView < ApplicationPhlexComponent
	# The ApplicationView is an abstract class for all your views.

	# By default, it inherits from `ApplicationPhlexComponent`, but you
	# can change that to `Phlex::HTML` if you want to keep views and
	# components independent.
end
