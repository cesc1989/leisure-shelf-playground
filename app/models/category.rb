class Category < ApplicationRecord
  has_many :activities

  validates :name, presence: true

  # Mira la documentación sobre los macros broadcast_to_x en
  # https://rubydoc.info/github/hotwired/turbo-rails/Turbo/Broadcastable/ClassMethods
  #
  # En la documentación en el código fuente se encuentran los broadcast_X_to
  # https://github.com/hotwired/turbo-rails/blob/main/app/models/concerns/turbo/broadcastable.rb
  after_create_commit { broadcast_append_to "categories" }
  after_update_commit { broadcast_replace_to "categories" }
end

# == Schema Information
#
# Table name: categories
#
#  id          :integer          not null, primary key
#  name        :string
#  description :string
#  cover       :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
