class Activity < ApplicationRecord
  belongs_to :category, optional: true
  belongs_to :shelf, optional: true

  validates :name, presence: true
  validates :description, presence: true
end

# == Schema Information
#
# Table name: activities
#
#  id          :integer          not null, primary key
#  kind        :integer
#  name        :string
#  description :text
#  category_id :integer
#  shelf_id    :integer
#  tags        :string
#  author      :string
#  cover       :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_activities_on_category_id  (category_id)
#  index_activities_on_shelf_id     (shelf_id)
#
# Foreign Keys
#
#  category_id  (category_id => categories.id)
#  shelf_id     (shelf_id => shelves.id)
#
