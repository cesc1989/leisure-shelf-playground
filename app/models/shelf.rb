class Shelf < ApplicationRecord
  has_many :activities

  validates :name, presence: true

  after_create_commit { broadcast_append_to "shelves" }
  after_update_commit { broadcast_replace_to "shelves" }
end

# == Schema Information
#
# Table name: shelves
#
#  id          :integer          not null, primary key
#  name        :string
#  description :string
#  cover       :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
