# frozen_string_literal: true

class ShelfDetailComponent < ApplicationComponent
  def initialize(shelf_detail:)
    @shelf = shelf_detail
    @activities = @shelf.activities
  end
end
