# frozen_string_literal: true

class ActivityCardComponent < ApplicationComponent
  def initialize(activity_card:)
    @activity = activity_card
  end
end
