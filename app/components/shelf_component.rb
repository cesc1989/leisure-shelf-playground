# frozen_string_literal: true

class ShelfComponent < ApplicationComponent
  def initialize(shelf:)
    @shelf = shelf
  end
end
