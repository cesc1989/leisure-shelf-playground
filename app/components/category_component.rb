# frozen_string_literal: true

class CategoryComponent < ApplicationComponent
  def initialize(category:)
    @category = category
  end
end
