# frozen_string_literal: true

class CategoryDetailComponent < ApplicationComponent
  def initialize(category_detail:)
    @category = category_detail
    @activities = @category.activities
  end
end
