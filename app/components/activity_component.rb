# frozen_string_literal: true

class ActivityComponent < ApplicationComponent
  def initialize(activity:)
    @activity = activity
    @tags = activity.tags
  end

  private

  def tag_list
    return [] unless @tags
    return [] if @tags.empty?

    @tags.split(',')
  end
end
