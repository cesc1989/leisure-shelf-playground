module ApplicationHelper
  def add_form_toggle_controller(params)
    return "data-controller=form-toggle" if ['categories', 'shelves'].include?(params[:controller])
  end
end
