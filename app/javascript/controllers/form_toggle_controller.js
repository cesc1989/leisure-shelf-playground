import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
/**
  El target debe definirse en esta lista y luego se accede
  mediante la instancia.

  La convención es:

    nameTarget

  ejemplo

    formTarget
    sourceTarget
*/
  static targets = ['form']

  toggle() {
    this.formTarget.classList.toggle('hidden');
  }
}
