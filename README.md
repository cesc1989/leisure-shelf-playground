# Leisure Shelf Playground

Una aplicación Ruby on Rails 7 de tipo estantería de cosas de ocio como libros, películas, series, etc.

Netamente de tipo informativo pero con la finalidadd de poder probar cosas nuevas que se vayan dadndo en el mundo Ruby on Rails.

Aquí pretendo poner a prueba conceptos, librerías y cualquier otra cosa que se pueda hacer en Rails.

## ¿Qué se prueba ahora mismo?

- [Tailwind CSS](https://tailwindcss.com/)
- [Stimulus](https://stimulus.hotwired.dev/)
- [Turbo](https://turbo.hotwired.dev/)
- [View Component](https://viewcomponent.org/)
- [Importmaps-rails](https://github.com/rails/importmap-rails/)

## ¿Qué se va a probar?

Tengo intenciones de probar:

- [Phlex](https://www.phlex.fun/)
- [Stimulus Reflex](https://docs.stimulusreflex.com/)
- [HTMX](https://htmx.org/)

## Instalación

Prerequisitos:

- Ruby 3.1.0
- Bundler
- SQLite3 (solo para desarrollo)

Clonar y ejecutar los comandos

- bundle install
- rails db:create
- rails db:schema:load
- rails db:seed

Para ejecutar el servidor usa el binario _dev_ presente en la carpeta `bin`

```
$ ./bin/dev
```
