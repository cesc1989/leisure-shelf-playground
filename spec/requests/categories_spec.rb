# spec/requests/categories_spec.rb

require 'rails_helper'

RSpec.describe "Categories", type: :request do
  describe "GET /index" do
    it "returns http success" do
      get "/categories"

      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /new" do
    it "returns http success" do
      get "/categories/new"

      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /edit" do
    it "returns http success" do
      category = create(:category)

      get "/categories/#{category.id}/edit"

      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /show" do
    it "returns http success" do
      category = create(:category)

      get "/categories/#{category.id}"

      expect(response).to have_http_status(:success)
    end
  end
end
