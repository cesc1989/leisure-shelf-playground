# spec/requests/shelves_spec.rb

require 'rails_helper'

RSpec.describe "Shelves", type: :request do
  describe "GET /index" do
    it "returns http success" do
      get "/shelves"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /show" do
    it "returns http success" do
      shelf = create(:shelf)

      get "/shelves/#{shelf.id}"

      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /new" do
    it "returns http success" do
      get "/shelves/new"
      expect(response).to have_http_status(:success)
    end
  end
end
