# spec/graphql/queries/query_activities_spec.rb

require 'rails_helper'

RSpec.describe 'Query Activities' do
  it 'returns existing activities' do
    create_list(:activity, 3)

    query = <<~GQL
      query {
        allActivities {
          id
          name
          description
        }
      }
    GQL

    result = LeisureShelfPlaygroundSchema.execute(query)

    expect(result).not_to be_empty
    expect(result['data']['allActivities'].first).to include('id', 'name', 'description')

    first_activity = result.dig('data', 'allActivities').first

    expect(first_activity.dig('name')).to eq('Hola Mundo')
  end
end
