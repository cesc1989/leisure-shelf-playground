require "rails_helper"

RSpec.describe ActivityCardComponent, type: :component do
  it "renders the activity card" do
    render_inline(described_class.new(activity_card: create(:activity)))

    expect(page).to have_css('div.name')

    # expect(find_field('div.name').value).to eq('Hola Mundo')
    # No sirve porque el método no está en #<RSpec::ExampleGroups::ActivityCardComponent:0x00007f8d75a10ae0>
  end
end
