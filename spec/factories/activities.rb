FactoryBot.define do
  factory :activity do
    name { 'Hola Mundo' }
    description { 'Vaya vaya' }
    cover { 'https://imagen.com/imagen.png' }
  end
end
