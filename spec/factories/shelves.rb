FactoryBot.define do
  factory :shelf do
    name { 'Hola Mundo' }
    description { 'Vaya vaya' }
    cover { 'https://imagen.com/imagen.png' }
  end
end
