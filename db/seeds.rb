if Category.count.zero?
  puts "Creando Seeds de Categorias"
  Category.create([
    { name: 'Ciencia Ficción' },
    { name: 'Fantasía' },
    { name: 'Historia' },
    { name: 'Ingenieria de Software' },
    { name: 'Desarrollo de Software' },
    { name: 'Mundo Agil' },
    { name: 'Manga' },
    { name: 'Anime' }
  ])
end

if Shelf.count.zero?
  puts "Creando Estantes"
  Shelf.create([
    { name: 'Manga' },
    { name: 'Libros Agiles' }
  ])
end

if Activity.count.zero?
  puts "Creando Actividades"
  Activity.create([
    {
      name: 'Imperios y Espadazos',
      description: 'Libro de Andonis Garrido narrando la historia de la humanidad',
      category_id: Category.find_by(name: 'Historia').id
    },
    {
      name: 'Product Design Sprint',
      description: 'Libro para entender como hacer MVPs',
      shelf_id: Shelf.find_by(name: 'Libros Agiles').id
    },
    {
      name: 'Getting Real',
      description: 'Libro de 37Signals sobre creación de productos web',
      category_id: Category.find_by(name: 'Desarrollo de Software').id
    },
    {
      name: 'Remote',
      description: 'Libro sobre cómo trabajar de manera remota'
    },
    {
      name: 'Steelheart',
      description: 'Los Reckoners. Libro de Brandon Sanderson.',
      category_id: Category.find_by(name: 'Fantasía').id
    },
    {
      name: 'Tintin y Milu',
      description: 'Libro animado de aventuras',
    },
    {
      name: 'Los Compas',
      description: 'Libro animado de aventuras',
    },
    {
      name: 'Mandalas',
      description: 'Para colorear',
    },
    {
      name: 'Code Complete',
      description: 'Sobre cómo hacer software',
      category_id: Category.find_by(name: 'Ingenieria de Software').id
    },
    {
      name: 'La Caída de los Gigantes',
      description: 'La historia de la primera guerra mundial desde una perspectiva diferente',
      category_id: Category.find_by(name: 'Historia').id
    },
    {
      name: 'Castlevania',
      description: 'Serie animada sobre el vampiro Alucard',
      category_id: Category.find_by(name: 'Anime').id
    },
    {
      name: 'Gunbuster',
      description: 'Serie de mechas que deben enfrentar el fin de la raza humana',
      category_id: Category.find_by(name: 'Anime').id
    },
    {
      name: 'Saiki',
      description: 'Anime de humor con un protagonista irreverent',
      category_id: Category.find_by(name: 'Anime').id
    },
    {
      name: 'The Boys',
      description: 'Serie donde los superheroes no son tan buenos'
    }
  ])
end
