class CreateActivities < ActiveRecord::Migration[6.0]
  def change
    create_table :activities do |t|
      t.integer :kind
      t.string :name
      t.text :description
      t.references :category, null: true, foreign_key: true
      t.references :shelf, null: true, foreign_key: true
      t.string :tags
      t.string :author
      t.string :cover

      t.timestamps
    end
  end
end
